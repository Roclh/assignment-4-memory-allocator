#ifndef MEMORY_ALLOCATOR_MEMORYTEST_H
#define MEMORY_ALLOCATOR_MEMORYTEST_H

#include <stdbool.h>

bool test_region_border(FILE* log);

bool test_mem_alloc(FILE* log);

bool test_free_one_block(FILE* log);

bool test_free_two_blocks(FILE* log);

bool test_extend_region_close(FILE* log);

bool test_extend_region_somewhere(FILE* log);


#endif //MEMORY_ALLOCATOR_MEMORYTEST_H
